---
title: 0050 Official WSL image for Arch Linux
---

# Official WSL image for Arch Linux

- Date proposed: 2025-02-11
- RFC MR: <https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/50>

## Summary

Provide an official Arch Linux WSL image.

The WSL images are hosted on our `geo` mirrors.  
We plan to include this image in Microsoft's [official distribution manifest](https://github.com/microsoft/WSL/blob/master/distributions/DistributionInfo.json) to simplify the installation process for users.  
We do **not** intend to distribute this image through the Microsoft Store due to concerns about its terms of service and their implications for our trademarks.

With this official image, the WSL related wiki page will be accessible and editable again.

## Motivation

An official Arch Linux image for WSL ([Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/about)) would provide an easy way for people to give Arch Linux a try directly from their Windows system, making our distribution more "discoverable" and accessible.

Additionally, some people may have no other choice but to use Windows in specific situations (despite not necessarily being the best choice for their needs / duties), e.g. on their company's workstation.
While WSL consists of an acceptable solution to get access to a Linux environment in such situations, the lack of an official image for Arch Linux either "suggests" the use of a different distribution or "forces" people to rely on one of the many unofficial Arch Linux WSL images, which quality may vary.

This overall topic has already been discussed and mostly hashed out in the [related mail thread](https://lists.archlinux.org/archives/list/arch-dev-public@lists.archlinux.org/thread/73A4BK7YK4BJBVXGMN2I5CROQAWI53VZ/) in the Arch-Dev-Public mailing list.

The proposal for an Arch Linux WSL image was inspired by [the related Fedora one](https://fedoraproject.org/wiki/Changes/FedoraWSL).

## Specification

Most specifications have been exposed in [this mail](https://lists.archlinux.org/archives/list/arch-dev-public@lists.archlinux.org/message/XXBKAOZMKD5SFAM2PVFBKOAS57VENYPA/).  
The following sections will summarize the key takeaways from the mailing list thread.

### Maintenance

The WSL image maintenance is done in a [dedicated GitLab repository](https://gitlab.archlinux.org/archlinux/archlinux-wsl) under the `archlinux` namespace.

It contains all scripts and files needed to create a WSL image for Arch Linux, built from scratch.  
Images are automatically built monthly and published in the GitLab package registry via a scheduled GitLab CI.
It also automatically creates a tag and a release containing a link to the image (as well as a `sha256` checksum file for it) as release assets.

The repository also contains documentation on how to build your own image (which is pretty straightforward).

Overall, the maintenance should be pretty low as most tasks are automated.

### Distribution

We intend to host / sync WSL images to our `geo` mirrors (like it's currently done for our ISO & "arch-boxes" images).  
In terms of retention, we intend to keep the last three images around.
As images are built monthly, this is equal to a 3 months retention period (like it's currently done for our ISO & "arch-boxes" images).  
Once downloaded, users only have to double click it (or run a single `PowerShell` command, depending on the WSL version the system is running) to install it.

We also intend to add this image to the Microsoft's [official distribution manifest](https://github.com/microsoft/WSL/blob/master/distributions/DistributionInfo.json) for WSL for which contact and specification [have already been established](https://github.com/microsoft/WSL/issues/12551) with the Microsoft WSL team.  
Monthly pull requests required to keep the image updated in Microsoft's distribution manifest will be automated (like it's currently done for our Docker image).  
This allows users to download and install the latest WSL Arch Linux image via a single `PowerShell` command (`wsl --install archlinux`).

Due to required policies & TOS agreements and the concerns they raise around the implied permission of our trademark usage, we do **not** intend to distribute this image through the Microsoft Store.

### Technical support

Technical support will be provided at a "best effort" level with low priority **from the dedicated GitLab repository** & **only for WSL2** (WSL1 will **not** be supported).

We don't expect Arch staff (e.g. Bug Wranglers and Package Maintainers) to provide support if they can't and / or don't want to. Some community involvement regarding support in the dedicated WSL repository is however expected (the related mailing list thread already got a few answers from some people from the community that expressed their desire to help in that regard).

We also intend to remove the [redirection currently applied to the WSL related wiki page](https://wiki.archlinux.org/index.php?title=Install_on_WSL&oldid=724105) in order to allow the community to share and contribute WSL related content there (including the classic "Tips and tricks" and "Troubleshooting" sections).

## Drawbacks

This represents an additional image to maintain, host and support.  

However, the overall maintenance and hosting steps are automated and should therefore not represent any considerable effort.
The required disk space usage is also fairly low (one image currently weights about `150 MiB`).

When it comes to technical support, it is totally fair that some person from the staff are not interested nor willing to provide support for such a WSL image.
But providing technical support from the dedicated GitLab repository would allow to keep every WSL related issues / bug reports in a dedicated space, preventing staff members with no interest in supporting WSL from stumbling across them.  
Moreover, for common usage, I don't think much WSL specific issues (that are actual Arch Linux issues and not upstream WSL ones that is) are expected.
Still, restricting the support scope to WSL2 and at a "best effort" level should *hopefully* keep the amount and the scope of bug reports reasonable.  
As said earlier, we expect some community involvement on that front.

## Unresolved Questions

None.

## Alternatives Considered

Either not distribute an official WSL image (as it is the case today) or eventually distributing such a WSL image under my own name / namespace as *yet another* unofficial image.
