---
title: 0043 Streamline the RFC writing process
---

# Streamline the RFC writing process

- Date proposed: 2024-08-30
- RFC MR: <https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0043>

## Summary

Streamline the writing and publishing of RFCs by relying on markdown and dedicated spellcheckers and linters.

## Motivation

[ReStructuredText](https://en.wikipedia.org/wiki/ReStructuredText) (RST) is currently used for writing and publishing [Arch Linux RFCs](https://rfc.archlinux.page/).
While used in specific ecosystems (e.g.  Python) and in publishing, it is considered less widespread and more exotic than [markdown](https://en.wikipedia.org/wiki/Markdown).

Markdown is widely adopted across many platforms and tools, including [GitLab](https://gitlab.com/) and [Hugo](https://gohugo.io/) (which is used for generating the static website at <https://rfc.archlinux.page/>).
Additionally, a large set of linters and check tools exist for it and due to its widespread use, many people have a basic understanding of how to use it.

Many RFC drafts start out in pads using our [HedgeDoc](https://md.archlinux.org) instance, which allows collaborative writing of markdown documents.
Converting these drafts to RST before introducing them on the [issue tracker](https://gitlab.archlinux.org/archlinux/rfcs) is tedious and error-prone, especially for long RFCs.

GitLab's integration for rendering RST is subpar and as such many [directives](https://docutils.sourceforge.io/docs/ref/rst/directives.html) are not rendered correctly when accessing the page of an RFC document in a merge request.
Contributors and reviewers have to rely on building the resulting website for a clear review path, which is time consuming.

A large part of the Arch Linux team does not consist of English native speakers.
Frequently we see the need for various spelling and grammar fixes in the RFC review process.
This may be discouraging to the people writing the RFC and should be streamlined with automation.

Overall, preparing an RFC may be quite intimidating and tedious, both on the grounds of missing automation for correcting spelling issues and due to not being familiar with RST.

## Specification

In an initial step, we will convert the current RST-based [RFC template](https://gitlab.archlinux.org/archlinux/rfcs/-/blob/9e21bf019ad75e294698b27abb243edf00b9d25d/rfcs/0000-template.rst) to markdown and adapt integration accordingly.

In a second iteration we will add a configurable spellchecker ([`typos`](https://github.com/crate-ci/typos)), a code linter ([`markdownlint`](https://github.com/markdownlint/markdownlint)) and a prose linter ([`vale`](https://github.com/errata-ai/vale)), which will be integrated with the central build system and CI of the project.
The markdown line length check is disabled to facilitate having a single sentence per line, making diffs easier to review and avoiding text rewrap when changes occur in a hunk.

The following configuration items should be disabled for [`markdownlint`](https://github.com/markdownlint/markdownlint):

- [MD002](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md002---first-header-should-be-a-top-level-header)
  (first header should be a top header, for `hugo` top tags)
- [MD013](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md013---line-length)
  (line length)
- [MD024](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md024---multiple-headers-with-the-same-content)
  (multiple headers with the same content)
- [MD041](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md041---first-line-in-file-should-be-a-top-level-header)
  (first line in file should be a top level header, for `hugo` top tags)

Implementers will evaluate improvements of the configuration on the fly.

The [`vale`](https://github.com/errata-ai/vale) configuration will be generated initially and expanded upon iteratively.

In a final step we will convert all existing RFCs from RST to markdown, which has the added benefit of not using two differing systems in the long term.

## Drawbacks

- Spellcheckers and prose linters may raise "false positives" without proper configuration.

- Respective configuration files have to be maintained and potentially extended with each new RFC by e.g. adding new technical terms.

- Prose linters may require more in-depth configuration (initially or short to midterm) to arrive at a good result, which implies an iterative configuration process.

- Additional linter rules may be added upstream and will need evaluation when using them in the RFC project.

## Unresolved Questions

None.

## Alternatives Considered

- Only adding an additional markdown version of the RFC template would offer people to choose markdown or RST when writing RFCs.

- However, this leads to higher maintenance costs long-term if two systems are used and have to be maintained.

- Additionally, further using RST comes with the downside of poor rendering on GitLab and worse linting integration.
