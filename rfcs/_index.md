---
type: docs
---

# Request for Comment

A Request for Comment ([RFC](https://en.wikipedia.org/wiki/Request_for_Comments)) is a way for Arch Linux contributors to propose, design and discuss new features and changes in project direction in a focused environment.

RFCs start as [merge requests](https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests) in GitLab.
Once accepted, they can be found in the [RFCs folder](https://gitlab.archlinux.org/archlinux/rfcs/-/tree/master/rfcs).
More details about the RFC process can be found in the [README.md](https://gitlab.archlinux.org/archlinux/rfcs/-/blob/master/README.md) file.
