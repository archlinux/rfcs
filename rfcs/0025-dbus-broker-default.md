---
title: 0025 Make dbus-broker our default D-Bus daemon
---

# Make dbus-broker our default D-Bus daemon

- Date proposed: 2023-12-08
- RFC MR: <https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/25>

## Summary

Make `dbus-broker` our default D-Bus daemon, while still allowing the venerable `dbus-daemon` to be used and minimizing the disruption of existing installations.

## Motivation

`dbus-broker` provides better performance and higher reliability than `dbus-daemon`, with per-user accounting of resources in the broker.
For more information, see the [dbus-broker wiki](https://github.com/bus1/dbus-broker/wiki).

It integrates better with `systemd`, asking the manager to start transient services when D-Bus services define `systemd` unit to be activated, avoiding launching services into the D-Bus daemon\'s `cgroup`.

The service\'s environment is the manager\'s environment.
This replaces the distinction between dbus-daemon\'s launch environment and `systemd`\'s environment.

Fedora 30 switched to `dbus-broker` in 2019.
Their [change proposal](https://fedoraproject.org/wiki/Changes/DbusBrokerAsTheDefaultDbusImplementation) also includes a detailed rationale.

## Specification

- Move `dbus-broker` to `[core]`.

- Add `dbus-broker-units` and `dbus-daemon-units` containing the respective `dbus.service` system and user units for the implementation, each providing `dbus-units` and conflicting with the other.

- Make `systemd` depend on `dbus-units` to ensure a provider is installed.

- Make a news post informing users about the change:

  ``` markdown
  We are making `dbus-broker` our default implementation of D-Bus, for
  improved performance, reliability and integration with systemd.

  For the foreseeable future we will still support the use of `dbus-daemon`,
  the previous implementation. Pacman will ask you whether to install
  `dbus-broker-units` or `dbus-daemon-units`. We recommend to pick the
  default.

  For a more detailed rationale, please see our [rfc0025][1].

  [1]: https://gitlab.archlinux.org/archlinux/rfcs/-/blob/master/rfcs/0025-dbus-broker-default.rst
  ```

## Drawbacks

We force the user to select an implementation on upgrade and on each new installation.
However, the default selection (when pressing Enter) will be `dbus-broker-units` because of the lexicographic ordering.

## Unresolved Questions

None.

## Alternatives Considered

Making `dbus-units` an empty package depending on `dbus-broker-units` and setting up `replaces` to install either `dbus-broker-units` or `dbus-daemon-units` on existing systems.
This would avoid asking the user on a new installation, but I could not get the `replaces` to work properly.

The status quo is that `dbus-broker` has to be installed and its services enabled manually.
